# Teltonika FM-XXXX TCP Encoder/decoder

Protocol Documentation 
https://wiki.teltonika.lt/view/Codec

Implementation for Codec 8
https://wiki.teltonika.lt/view/Codec_8

Device
https://wiki.teltonika.lt/view/FMB120

I/O element Definition
https://wiki.teltonika.lt/view/FMB_AVL_ID

Links to libraries whose code was used
https://github.com/uro/teltonika-fm-parser

https://github.com/cf-git/teltonika-functions/blob/master/teltonika.php
